# Дієвидло
## (Наразі проєкт закинутий :( )
**For English [README](https://gitlab.com/imbiruss/dievidlo/-/blob/master/README_ENG.md)**
Бот для Discord що б стежити за граматикою учасників.

***Наразі є тільки українська та англійська для перевірки і вони обираються при команді /setup.***

**Буду вдячний pull request , адже кількість мов у бота залежить від [language tool](https://languagetool.org/uk).**

## Чому саме Дієвидло?
~~Автор цього боту трохи не використовує мізки...~~

Якщо чесно сам не знаю. Але якщо вам цікаво, докладно [тут](https://sbt.localization.com.ua/article/diyevidlo-i-ne-tilki/).
## Цілі
- [ ] Дописати README
- [x] Інші мови для перевірки
## Запуск
Треба встановити [Docker](https://www.docker.com/) та створити необхідний контейнер, **ВАЖЛИВО** змінити пароль на свій , за бажанням можна змінити порт та юзера в ДБ.
```
docker pull postgres
```
```
docker run --name postgres-container -e POSTGRES_PASSWORD=your_password -p 5432:5432 -d postgres
//після того як ви виконаєте цю команду вам не треба її ще раз вводити
```
Далі встановити [Rust](https://www.rust-lang.org/) та languagetool-rust.
```
cargo install languagetool-rust --features full
```
Для роботи бота потрібно завжди запустити скрипт який запускає контейнери для роботи бота.
```
sudo sh ./run-containers.sh
```
Отримайте токен Discord бота створивши його на [порталі для розробників](https://discord.com/developers/applications).

В репозиторії є [Config.toml](https://gitlab.com/imbiruss/dievidlo/-/blob/master/Config.toml) де **ОБОВ'ЯЗКОВО** треба вписати необхідні дані.
```toml
[config]
host = "localhost" #хост на якому працює postgres
port = 5432 #порт на якому працює postgres
passw = "" #пароль в postgres
usr = "postgres" #користувач в postgres
token = ""  #токен Discord бота
```
Далі запускаємо самого бота.
```
cargo run --release
```
