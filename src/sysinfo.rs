pub mod sys {
    use sysinfo::{System, SystemExt};

    pub async fn get_current_sys() -> String {
        let mut sys = System::new_all();
        sys.refresh_all();
        format!(
            "
Системні дані на даний момент:
**ОЗП**: Зайнято {:.3} GB з {:.3} GB
**СВАП**: Зайнято {:.3} GB з {:.3} GB
**Час роботи**: {:.3} годин
**Середнє навантаження**: {}%

**Назва ОС**: {}
**Версія**: {}
**Версія ядра**: {}
",
            sys.used_memory() as f64 / 1073741824f64,
            sys.total_memory() as f64 / 1073741824f64,
            sys.used_swap() as f64 / 1073741824f64,
            sys.total_swap() as f64 / 1073741_824f64,
            sys.uptime() as f64 / 3600.0,
            sys.load_average().one,
            sys.name().unwrap_or("Unknown name".to_string()),
            sys.os_version().unwrap_or("Unknown version".to_string()),
            sys.kernel_version()
                .unwrap_or("Unknown kernel version".to_string())
        )
    }
}
