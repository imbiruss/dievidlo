pub mod grammar {
    use languagetool_rust::{CheckRequest, ServerClient};
    use serenity::model::prelude::Message;
    pub async fn check_msg(lt_client: &ServerClient, msg: &Message, lang: String) -> Result<String, ()> {
        // Returning generic error type
        let req = CheckRequest::default()
            .with_text(msg.content.to_string())
            .with_language(lang);

        let res = lt_client.check(&req).await.expect("expect to check");
        let mut pretty_res: String = String::new();
        for match_err in res.iter_matches() {
            let mut replacements: String = String::new();
            for word in match_err.replacements.iter() {
                replacements.push_str(format!("*{}* ", word.value.as_str()).as_str());
            }
            pretty_res.push_str(
                format!(
                    "
**Помилка**: {}
**Можливі заміни**: {}\n",
                    match_err.short_message, replacements
                )
                .as_str(),
            )
        }
        Ok(pretty_res)
    }
}
