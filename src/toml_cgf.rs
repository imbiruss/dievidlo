pub mod toml_cfg {
    use serde_derive::Deserialize;
    use tokio::fs;
    use toml;
    #[derive(Deserialize)]
    struct Data {
        config: Config,
    }
    #[derive(Deserialize)]
    pub struct Config {
        pub host: String,
        pub port: u16,
        pub passw: String,
        pub usr: String,
        pub token: String,
    }
    const FILE_NAME: &str = "Config.toml";
    pub async fn get_cfg() -> Config {
        let contents = fs::read_to_string(FILE_NAME)
            .await
            .expect("Expect to read Config.toml");
        let data: Data = toml::from_str(&contents).expect("expect to parse data");
        let cfg = Config {
            host: data.config.host,
            port: data.config.port,
            passw: data.config.passw,
            usr: data.config.usr,
            token: data.config.token,
        };
        cfg
    }
}
