    use serenity::model::prelude::Message;
    use tokio_postgres::Client;
    pub struct Server {
        pub guild: i64,
        pub channels: Option<Vec<i64>>,
        pub lang: String,
    }

    impl Server {
        pub async fn new(client: &Client, guild: i64) -> Option<Self> {
        let rows = client.query("SELECT * FROM public.dievidlo_servers WHERE server_id=$1;", &[&guild]).await.unwrap();
            if let Some(row) = rows.iter().next() {
                let guild: i64 = row.get("server_id");
                let channels: Option<Vec<i64>> = row.get("channels");
                let lang: Option<String> = row.get("lang");
                let server = Server {
                    guild,
                    channels: channels,
                    lang: lang.unwrap_or_default(),
                };
             Some(server)
            } else {
                None
            }

        }
        pub async fn new_for_setup(client: &Client, guild: i64) -> Option<Self> {
            Some(Server {
                guild,
                channels: None,
                lang: "".to_string(),
            })
        }

    pub async fn get_channels(&self, client: &Client) -> Result<Vec<i64>, ()> {
        let res = match client
            .query_one(
                "SELECT channels FROM public.dievidlo_servers where server_id=$1;",
                &[&self.guild],
            )
            .await
        {
            Ok(value) => value,
            Err(err) => {
                eprintln!("Failed to query db: {:?}", err);
                return Err(());
            }
        };
        let channels: Vec<i64> = res.get::<_, Vec<i64>>(0);
        Ok(channels)
    }
    pub async fn add_channel(&self, client: &Client, channel_id: i64) -> Result<(), ()> {
        if (client.execute("update public.dievidlo_servers set channels = array_append(channels, $1) where server_id=$2;", &[&channel_id, &self.guild]).await).is_ok(){
            println!("Channel {} added from {} guild", channel_id, self.guild);
            Ok(())
        } else {
            println!("Something went wrong! Channel: {}, Guild: {}", channel_id, &self.guild);
            Err(())
        }
    }
    pub async fn rm_channel(&self ,client: &Client, channel_id: i64) -> Result<(), ()> {
        if (client.execute("update public.dievidlo_servers set channels = array_remove(channels, $1) where server_id=$2;", &[&channel_id, &self.guild]).await).is_ok(){
            println!("Channel {} is remove", channel_id);
            Ok(())
        } else {
            Err(())
        }
    }
    pub async fn get_lang_for_ltrs(&self) -> String {
        return match self.lang.as_str() {
           "ukr" => "uk-UA".to_string(),
            "eng"=> "en".to_string(),
            _ => "".to_string()
        }
    }
        pub async fn check_in_db(&self, client: &Client, msg: &Message) -> Result<bool, ()> {
            let guild_id = *msg.guild_id.expect("expect to get guild_id").as_u64() as i64;
            let channel_id = *msg.channel_id.as_u64() as i64;
            return match &self.get_channels(&client).await {
                Ok(channels) => {
                    if channels.contains(&channel_id) {
                        println!("Channel {} exists in DB of this server , channels for monitoring on this server {:?}", channel_id, channels);
                        Ok(true)
                    } else {
                        Ok(false)
                    }
                },
                Err(_) => {
                    log::error!("Failed to get channels from {}", &guild_id);
                    Ok(false)
                }
            };
        }
}

    pub async fn setup_in_db(
        client: &Client,
        guild_id: i64,
        lang: String,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let result = client
            .execute(
                "INSERT INTO dievidlo_servers (server_id, lang)
                SELECT $1 , $2
                WHERE NOT EXISTS (
                SELECT 1 FROM dievidlo_servers WHERE server_id = $1
                );",
                &[&guild_id, &lang],
            )
            .await;

        match result {
            Ok(_) => {
                println!("Guild {} is set up", guild_id);
                Ok(())
            }
            Err(e) => {
                println!("Failed to insert server in table {:?}", e);
                Err(Box::new(e)) // structurally preserve the error and pass it along
            }
        }
    }
