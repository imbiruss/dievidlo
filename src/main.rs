mod command;
mod db;
mod lt;
mod sysinfo;
mod toml_cgf;


use std::time::Duration;
use crate::db::{Server};
use crate::lt::grammar::check_msg;
use languagetool_rust::{ServerClient};
use rand::Rng;
use serenity::{
    async_trait,
    model::application::command::Command,
    model::application::interaction::{Interaction, InteractionResponseType},
    model::channel::Message,
    model::gateway::Ready,
    prelude::*,
};
use serenity::model::gateway::Activity;
use tokio_postgres::{Client, Config, NoTls};
use toml_cgf::toml_cfg;
struct Handler {
    client: Client,
    lt_client: ServerClient, //transferring clients to implementation
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.author.bot {
        } else {
            let server = Server::new(&self.client, *msg.guild_id.unwrap_or_default().as_u64() as i64).await.unwrap();
            if let Ok(true) = server.check_in_db(&self.client, &msg).await {
                if let Ok(res) = check_msg(&self.lt_client, &msg, server.get_lang_for_ltrs().await.to_string()).await {
                    if res.is_empty() {
                    } else {
                        msg.reply(ctx.http, &res).await.unwrap_or_else(|e| {
                            log::error!("An error occurred: {:?} , {}", e, &res);
                            msg
                        });
                    }
                }
            } else {
            }
        }
    }
    async fn ready(&self, ctx: Context, ready: Ready) {
        //when the bot is ready, load everything you need
        println!("{} bot connected", ready.user.name);
        let commands = match Command::set_global_application_commands(&ctx.http, |commands| {
            commands
                .create_application_command(|cmd| command::channel_id::register(cmd))
                .create_application_command(|cmd| command::setup::register(cmd))
                .create_application_command(|cmd| command::rm_channel::register(cmd))
                .create_application_command(|cmd| command::sysinfo::register(cmd))
        })
        .await {
            Ok(c) => c,
            Err(e) => {log::error!("Error register commands: {}" ,e);
            return;
            },
        };// registering bot commands
        random_shitpost(&ctx).await;
    }
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        // reacting to the call of the slash command
        if let Interaction::ApplicationCommand(command) = &interaction {
            println!("Received commands: {:#?}", command);
            let server = Server::new_for_setup(
                &self.client,
                *command.guild_id.unwrap_or_default().as_u64() as i64
            ).await.expect("Expected server struct.");
            if command.data.name == "setup" {
                command::setup::run(&self.client, &ctx, &interaction, &command, server.guild).await;
            } else {
                let content = match command.data.name.as_str() {
                    // determine which command it is
                    "add-channel" => {
                        command::channel_id::run(
                            &command.data.options,
                            server,
                            &self.client,
                        )
                            .await
                    },
                    "rm-channel" => {
                        command::rm_channel::run(
                            &command.data.options,
                            server,
                            &self.client,
                        )
                            .await
                    },
                    "sysinfo" => command::sysinfo::run(&command.data.options).await,
                    _ => "".to_string(),
                };
                let response = command.create_interaction_response(&ctx.http, |resp| {
                    resp.kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|m| m.content(content).ephemeral(true))
                }).await;
                tokio::time::sleep(Duration::from_secs(3)).await;
                command.delete_original_interaction_response(&ctx.http).await.unwrap_or_default();
                if let Err(why) = response {
                    log::error!("Error sending response {}", why);
                }
            }
        }
    }
}
#[tokio::main]
async fn main() {
    let intents = GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT;
    let mut cfg = Config::new();
    let temp_cfg = toml_cfg::get_cfg().await;
    cfg.user(&temp_cfg.usr);
    cfg.password(&temp_cfg.passw);
    cfg.host(&temp_cfg.host);
    cfg.port(temp_cfg.port);
    let (client, connection) = Config::connect(&cfg, NoTls)
        .await
        .expect("expect to connect db");
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    }); // check for errors
    client
        .execute("CREATE DATABASE dievidlo_db;", &[])
        .await
        .unwrap_or_else(|e| {
            if e.code() == Some(&tokio_postgres::error::SqlState::DUPLICATE_DATABASE) {
                // if it's a duplicate error, we ignore it
                println!("DB already exists...");
                1
            } else {
                // if something went wrong then we panic
                panic!("Failed to create DB: {:?}", e);
            }
        });
    client
        .execute(
            "CREATE TABLE public.dievidlo_servers (
server_id BIGINT,
channels BIGINT[] NULL,
lang VARCHAR(3) NULL
        );",
            &[],
        )
        .await
        .unwrap_or_else(|e| {
            if e.code() == Some(&tokio_postgres::error::SqlState::DUPLICATE_TABLE) {
                println!("Table already exists...");
                1
            } else {
                panic!("Failed to create Table: {:?}", e);
            }
        });
    let lt_client = ServerClient::new("http://localhost", "8010");
    println!(
        "Ping: {}",
        lt_client.ping().await.expect("Expect to ping lt server")
    );
    let handler = Handler { client, lt_client };
    let mut client = serenity::Client::builder(&temp_cfg.token, intents)
        .event_handler(handler)
        .await
        .expect("Error creating client");
    // make a client
    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
    // start
}
async fn random_shitpost(ctx: &Context) {
    loop {
        let status: Vec<&str> = vec![
            "Сьогодні я забороняю тобі какати!",
            "А ти знаєш що я блокую 'шо'?",
            "івзєапршолжвдпелрохзкеірщшг",
            "Присядь 20 раз... НЕГАЙНО!",
            "Мій син imbirwithoutsugar."
        ];
        let curr_time = chrono::Local::now();
        let next_d = curr_time.date().succ();
        let midnight = next_d.and_hms(0,0,0,);
        let dur_until_midn = midnight.signed_duration_since(curr_time);
        let random_status = status[rand::thread_rng().gen_range(0..status.len())];
        ctx.set_activity(Activity::listening(random_status)).await;
        tokio::time::sleep(dur_until_midn.to_std().unwrap_or_default()).await;
    }
}