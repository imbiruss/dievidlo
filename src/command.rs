pub mod channel_id {
    use serenity::{
        builder::CreateApplicationCommand,
        model::application::command::CommandOptionType,
        model::prelude::interaction::application_command::{
            CommandDataOption, CommandDataOptionValue,
        },
        model::Permissions,
    };
    use tokio_postgres::Client;
    use crate::db::Server;

    pub async fn run(opts: &[CommandDataOption], server: Server, client: &Client) -> String {
        let options = opts
            .get(0)
            .expect("Expected user options")
            .resolved
            .as_ref()
            .expect("Expected user object");

        if let CommandDataOptionValue::Channel(channel) = options {
            if (server.add_channel(client, *channel.id.as_u64() as i64).await).is_ok() {
                format!("Channel <#{}> added", &channel.id.as_u64())
            } else {
                "Failed to add channel for monitoring".to_string()
            }
        } else {
            return "Incorrect option".to_string();
        }
    }
    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
        command
            .name("add-channel")
            .description("Add channel to monitor")
            .create_option(|opt| {
                opt.name("channel")
                    .description("Channel ID")
                    .kind(CommandOptionType::Channel)
                    .required(true)
            })
            .default_member_permissions(Permissions::ADMINISTRATOR) //only administrators can add channels
            .dm_permission(false) //prohibit commands in PM
    }
}

pub mod setup {
    use std::time::Duration;
    use serenity::{
        model::prelude::interaction::application_command::ApplicationCommandInteraction,
        builder::CreateApplicationCommand,
        model::Permissions,
        prelude::Context,
        utils::MessageBuilder,
        model::prelude::interaction::Interaction,
    };
    use serenity::builder::CreateComponents;
    use serenity::model::application::interaction::InteractionResponseType;
    use tokio_postgres::Client;
    use crate::db::setup_in_db;

    pub async fn run(
        client: &Client,
        ctx: &Context,
        interaction: &Interaction,
        command: &&ApplicationCommandInteraction,
        guild: i64
    ) {
        match command.create_interaction_response(&ctx.http , |r| {
            r.kind(InteractionResponseType::ChannelMessageWithSource)
                .interaction_response_data(|d|{
                    d.content("Please select a language:")
                        .components(|c| {
                            c.create_action_row(|row| {
                                row.create_select_menu(|menu| {
                                    menu.custom_id("lang_select");
                                    menu.placeholder("No language selected");
                                    menu.options(|f| {
                                        f.create_option(|o| o.label("Ukrainian").value("ukr"));
                                        f.create_option(|o| o.label("English").value("eng"))
                                    })
                                })
                            })
                        }).ephemeral(true)
                })
        }).await {
            Ok(_) => (),
            Err(e) => {
                log::error!("Error creating interaction response: {}", e);
                return;
            }
        }
        let m = match ctx.http.get_original_interaction_response(&interaction.token()).await {
            Ok(m) => m,
            Err(e) => {
                log::error!("Error getting original interaction response: {}", e);
                return;
            }
        };
        let interaction = match m.await_component_interaction(&ctx).await {
            Some(x) => x,
            None => {
                if let Err(e) = m.reply(&ctx, ":cow:").await {
                    log::error!("Error replying: {}", e);
                };
                return;
            },
        };
        let lang = interaction.data.values[0].as_str();
        match interaction.create_interaction_response(&ctx, |r| r.kind(InteractionResponseType::UpdateMessage).interaction_response_data(|d| {
            d.content(match lang {
                "ukr" => {
                    MessageBuilder::new()
                        .push_line("### Бот готовий до налаштування!")
                        .push_line("Всі команди для налаштування:")
                        .push_bold("/add-channel")
                        .push_line(": Додати канал до моніторингу.")
                        .push_bold("/rm-channel")
                        .push(": Видалити канал з моніторингу.")
                        .build()
                },
                "eng" => {
                    MessageBuilder::new()
                        .push_line("### The bot is ready for setup!")
                        .push_line("Here are all the setup commands:")
                        .push_bold("/add-channel")
                        .push_line(": Add a channel to monitor.")
                        .push_bold("/rm-channel")
                        .push(": Remove a channel from monitoring.")
                        .build()
                }
                _ => "Unknown".to_string()

            }).ephemeral(true).set_components(CreateComponents::default())
        })).await {
            Ok(_) => (),
            Err(e) => log::error!("Error creating interaction response: {}", e),
        };
        if let Err(e) = setup_in_db(client, guild, lang.to_string()).await {
            log::error!("Error setting up in db: {}", e);

        }
        tokio::time::sleep(Duration::from_secs(60)).await;
        if let Err(e) = interaction.delete_original_interaction_response(&ctx.http).await {
            log::error!("Error deleting original interaction response: {}", e)
        }
    }
    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
        command
            .name("setup")
            .description("Set up the bot automatically and learn commands")
            .default_member_permissions(Permissions::ADMINISTRATOR) //only administrators can add channels
            .dm_permission(false) //prohibit commands in PM
    }
}

pub mod rm_channel {
    use serenity::{
        builder::CreateApplicationCommand,
        model::application::command::CommandOptionType,
        model::prelude::interaction::application_command::{
            CommandDataOption, CommandDataOptionValue,
        },
        model::{Permissions},
    };
    use tokio_postgres::Client;
    use crate::db::Server;


    pub async fn run(opts: &[CommandDataOption], server: Server, client: &Client) -> String {
        let options = opts
            .get(0)
            .expect("Expected user options")
            .resolved
            .as_ref()
            .expect("Expected user object");

        if let CommandDataOptionValue::Channel(channel) = options {
            if (server.rm_channel(client, *channel.id.as_u64() as i64).await).is_ok() {
                format!("Channel <#{}> removed", &channel.id.as_u64())
            } else {
                "Failed to delete channel".to_string()
            }
        } else {
            "Incorrect option".to_string()
        }
    }

    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
        command
            .name("rm-channel")
            .description("Remove channel from settings")
            .create_option(|opt| {
                opt.name("channel")
                    .description("Channel ID")
                    .kind(CommandOptionType::Channel)
                    .required(true)
            })
            .default_member_permissions(Permissions::ADMINISTRATOR) //only administrators can remove channels
            .dm_permission(false) //prohibit commands in PM
    }
}
pub mod sysinfo {
    use crate::sysinfo::sys;
    use serenity::{
        builder::CreateApplicationCommand,
        model::prelude::interaction::application_command::CommandDataOption,
    };
    use sys::get_current_sys;

    pub async fn run(_opts: &[CommandDataOption]) -> String {
        get_current_sys().await
    }
    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
        command
            .name("sysinfo")
            .description("Provides system information")
            .dm_permission(false) //prohibit commands in PM
    }
}
