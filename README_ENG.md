# Dievydlo bot
## (The project is currently abandoned :( )
**For English [README](https://gitlab.com/imbirus/dievidlo/-/blob/master/README_ENG.md)**
A bot for Discord to monitor the grammar of participants.

***Currently, only Ukrainian and English are available for checking and they are selected with the /setup command.***

**I would be grateful for a pull request, as the number of languages in the bot depends on the [language tool](https://languagetool.org/uk)**.


## Why Dievydlo?
~~The author of this bot doesn't use his brain a bit...~~

To be honest, I don't know. But if you're interested, here's a detailed [description](https://sbt.localization.com.ua/article/diyevidlo-i-ne-tilki/).
## TODO
- [ ] Add README
- [x] Other languages for checking


## Starting
You need to install [Docker](https://www.docker.com/) and create the necessary container, **IMPORTANT** change the password to your own, optionally you can change the port and user in the DB.
```
docker pull postgres
```
```
docker run --name postgres-container -e POSTGRES_PASSWORD=your_password -p 5432:5432 -d postgres
//after you execute this command, you do not need to enter it again
```
Next, install [Rust](https://www.rust-lang.org/) and languagetool-rust.
```
cargo install languagetool-rust --features full
```
For the bot to work, you must always run a script that launches containers for the bot to work.
```
sudo sh ./run-containers.sh
```
Get the bot's Discord token by creating it on the [developer portal](https://discord.com/developers/applications).

In the repository there is a [Config.toml](https://gitlab.com/imbiruss/dievidlo/-/blob/master/Config.toml) where **MUST** enter the necessary data.
```toml
[config]
host = "localhost" # host on which postgres is running
port = 5432 #port on which postgres is running
passw = "" #password in postgres
usr = "postgres" #user in postgres
token = "" #Discord bot token
```
Next, we start the bot itself. 
```
cargo run --release
```
